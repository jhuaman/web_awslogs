$(document).ready(function() {
    

    var modal = document.getElementById("myModal"); 
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    span.onclick = function() 
    {
        modal.style.display = "none";
    }


    /**
     * Funcion para asignar un evento a los tab de estados y poder mostrar los trabajos de una cola de trabajo
     */
    obtenerlogs = function(status) {
        
        job_queues_id = $('select.jobQueueName :selected').text();    
        
        alert(job_queues_id);

        describe_lambdas_group(job_queues_id);

        var i, tabcontent, tablinks;

        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        document.getElementById(status).style.display = "block";

        $('#describe_job').html('');
        }


    /**
     * Funcion para asignar un evento a los tab de estados y poder mostrar los trabajos de una cola de trabajo
     */
    openCity = function(status) {
                
        job_queues_id = $('select.jobQueueName :selected').text();    
        
        describe_job_queues_status(job_queues_id,status);

        var i, tabcontent, tablinks;

        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        document.getElementById(status).style.display = "block";
        
        $('#describe_job').html('');
    }

    
    /**
     * Funcion para mostrar los trabajos de una cola de trabajo
     */
    describe_job_queues_status = function(job_queues_id,status)
    {						
        $.ajax({						
            url: ((stage) ? ('/' + stage) : '') + '/describe_job_queues_status/',
            data: {'job_queues_id':job_queues_id,'status':status},
            method: 'POST',
            success: function(data) {				
                $('#' + status).html(data['square']);
                console.log(data['square']);
            }
        });							
    }

    /**
     * Funcion para mostrar los trabajos de una cola de trabajo
     */
    describe_lambdas_group = function(job_queues_id)
    {						
        $.ajax({						
            url: '/' + stage + '/describe_lambda_group/',
            data: {'job_queues_id':job_queues_id},
            method: 'POST',
            success: function(data) {				
                $('#' + status).html(data['square']);
                console.log(data['square']);
            }
        });							
    }


    /**
     * Funcion para mostrar las colas de trabajo dando clic en el boton buscar
     */
    describe_job_queues = function()
    {			        
        $.ajax({	        					
            url:  ((stage) ? ('/' + stage) : '') + '/describe_job_queues/',
            data: {},
            method: 'POST',
            success: function(data) {                                        
                if(data){
                    $('#describe_job_queues').html(data['square'])
                    console.log(data['square'])	
                }
                else{
                    alert("The return value is null.");
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                alert("Status: " + textStatus + " - Error: " + errorThrown ); 
            }
        }).done( function() {            
            
        }).fail( function() {            
                        
        });

        ;								
    }

    

    /**
     * Funcion para mostrar las colas de trabajo dando clic en el boton buscar
     */
    describe_lambdas = function()
    {	
        $.ajax({						
            url:  ((stage) ? ('/' + stage) : '') + '/describe_lambdas/',
            data: {},
            method: 'POST',
            success: function(data) {						
                if(data){
                    $('#describe_job_queues').html(data['square']);
                    console.log(data['square']);	
                }
                else{
                    alert("The return value is null.");
                } 
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                alert("Status: " + textStatus + " - Error: " + errorThrown ); 
            }
        });								
    }

    /**
     * Funcion para mostrar los ids de logs de los trabajos
     */
    describe_job = function(jobid)
    {		
        $.ajax({						
            url: ((stage) ? ('/' + stage) : '') + '/describe_job/',
            data: {'jobid':jobid},
            method: 'POST',
            success: function(data) {				
                $('#describe_job').html(data['square']);
                console.log(data['square']);							
            }
        });	
    }

    
    /**
     * Funcion par mostrar el contenido del log por el id del log y el grupo al que pertenece
     */
    MyFunction = function(loggroupname_id,logstreamname_id)
    {
        if (loggroupname_id) {
            var loggroupname_name = loggroupname_id
        }
        else{
            var loggroupname_name = $('#loggroupname_id').val();
        }
                            
        var logstreamname_name = logstreamname_id;					
        var numberlogs_name = '';				
            
        $.ajax({						
            url: ((stage) ? ('/' + stage) : '') + '/square/',
            data: {'loggroupname_name': loggroupname_name,'logstreamname_name': logstreamname_name,'numberlogs_name':numberlogs_name},
            method: 'POST',
            success: function(data) {                                
                $('#textheader').html("LogName : " + logstreamname_name);                            						
                $('#textlog').html(data['square']);                   
                modal.style.display = "block";
            }
        });								
    }
});