#!/usr/bin/env python
import jinja2
import sys
from chalice import Chalice
from chalice import Response as chalice_response
from chalice import CORSConfig
import boto3
import tzlocal
from flask import Response as flask_response
from flask import Flask, render_template, request, jsonify
import flask
import time
from subprocess import call
import subprocess
from datetime import datetime
from datetime import timedelta
import time
import json
from pprint import pprint
import collections
from datetime import datetime as dt
from time import mktime
import os
import re
import datetime

ENV_NAME = os.getenv('stage','')
BUCKET = os.getenv('bucket','images-system')
app = Chalice(app_name='web_awslogs-{}'.format(ENV_NAME))

app_flask = Flask(__name__)

cors_config = CORSConfig(
    allow_origin='*',
    allow_headers=['X-Special-Header'],
    max_age=10080,
    expose_headers=['X-Special-Header'],
    allow_credentials=True
)

def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(loader=jinja2.FileSystemLoader(path or "./")).get_template(filename).render(context)

def get_headers():
    # via = 2.0 d7db6b9cdf796f6ea0cdbe5b3a75c84a.cloudfront.net (CloudFront)
    # cloudfront-is-desktop-viewer = true
    # cloudfront-is-smarttv-viewer = false
    # x-forwarded-port = 443
    # cloudfront-viewer-country = PE
    # x-amzn-trace-id = Root=1-5db12f88-669370dcea601fca8255b620
    # cloudfront-forwarded-proto = https
    # host = aw1p5h5zt2.execute-api.us-east-1.amazonaws.com        
    # x-forwarded-for = 190.237.229.145, 70.132.58.163
    # x-forwarded-proto = https
    # x-amz-cf-id = t9tzLu59YYDApSalHgKrkkNUBJnUucrAWp6S5oHLuuRhK8C452s8cA==
    # cloudfront-is-tablet-viewer = false
    # cloudfront-is-mobile-viewer = false

    # :authority: aw1p5h5zt2.execute-api.us-east-1.amazonaws.com # 
    # :path: /qa/
    # :scheme: https

    request = app.current_request
    headers = request.headers

    headers_new = {}

    if headers:
        for k in headers:
            headers_new[k] = headers[k]
    headers_new[':method'] = request.method

    for k in headers_new:
        print (k + " = " + headers_new[k])

@app.route('/',cors=cors_config)
def index():
        
    get_headers()
        
    """ Mostrar la pagina principal index.html
    """    
    context = {'BUCKET':BUCKET,'ENV_NAME':ENV_NAME}
    template = render("chalicelib/templates/index.html",context)
    return chalice_response(template, status_code=200, headers={"Content-Type": "text/html", "Access-Control-Allow-Origin": "*"})


@app_flask.route('/')
def index():
    """ Mostrar la pagina principal index.html
    """
    context = {'BUCKET':BUCKET,'ENV_NAME':ENV_NAME}
    return render_template('index.html',**context)

def get_describe_job_queues():
    """ Obtiene las descripciones de las colas de trabajo
    """
    batch = boto3.client('batch')
    queues = {q['jobQueueName']: q for q in batch.describe_job_queues()['jobQueues'] if q['state'] == 'ENABLED' and q['status'] == 'VALID'}    
    return sorted(list(queues.keys()))

def get_describe_lambdas():
    """ Obtiene las descripciones de las colas de trabajo
    """
    next_token = None

    client = boto3.client('logs')

    array_names = []

    while True:
        if next_token:
            response = client.describe_log_groups(logGroupNamePrefix='/aws/lambda/',nextToken=next_token)
        else:
            response = client.describe_log_groups(logGroupNamePrefix='/aws/lambda/')

        next_token = response.get('nextToken', None)

        for log_group in response['logGroups']:
            log_group_name = log_group['logGroupName']
            log_group_name = log_group_name.split("/")[-1]
            array_names.append(log_group_name)    
       
        if not next_token or len(response['logGroups']) == 0:
            break

    return sorted(array_names)


@app.route('/describe_job_queues', methods=['GET','POST'],cors=cors_config, content_types=['application/json'])
@app_flask.route('/describe_job_queues/',endpoint='flask_1',methods=['GET','POST'])
def describe_job_queues():    
    try:
        """ Muestra las descripciones de las colas de trabajo
        """
        cabecera = "<span style='color:red; font-weight: bold;'>"   
        
        informacion ="<form class='form-inline'><div class='form-group'>"
        informacion +="<label for='selection1'>Select Job Queue Name : </label>"
        
        informacion += "<select class='form-control jobQueueName' id='selection1'>"
        
        lines = get_describe_job_queues()
        for line in lines:
            try:            
                informacion+="<option value='" + str(line.rstrip()) + "'>" + str(line.rstrip()) + "</option>"
            except: 
                pass

        cabecera += "</span>"  

        informacion += "</select></div></form>"
        informacion += "<br><br><br>"
        informacion += "<div id='describe_job_queues_status'>"

        informacion += "<div class='tab'>\
        <button class='tablinks' onclick='openCity(\"SUBMITTED\")' id='defaultOpen'>SUBMITTED</button>\
        <button class='tablinks' onclick='openCity(\"PENDING\")'>PENDING</button>\
        <button class='tablinks' onclick='openCity(\"RUNNABLE\")'>RUNNABLE</button>\
        <button class='tablinks' onclick='openCity(\"STARTING\")'>STARTING</button>\
        <button class='tablinks' onclick='openCity(\"RUNNING\")'>RUNNING</button>\
        <button class='tablinks' onclick='openCity(\"SUCCEEDED\")'>SUCCEEDED</button>\
        <button class='tablinks' onclick='openCity(\"FAILED\")'>FAILED</button>\
        </div>"

        status = ['SUBMITTED','PENDING','RUNNABLE','STARTING','RUNNING','SUCCEEDED','FAILED']

        html_info = ""

        for stat in status:
                
            html_info += "<div id='" + stat + "' class='tabcontent'>"
            
            html_info += "<table id='t02'>"      
            html_info += "</table>"
                    
            html_info += "<br>"
            html_info += "<table id='t01'>"

            if stat == 'SUCCEEDED': 
                html_info += "<tr class='td_yes'><th>statusReason</th><th>jobName</th><th>stoppedAt</th><th>startedAt</th><th>jobId</th><th>createdAt</th><th>status</th><th>logs</th></tr>"
            elif stat == 'STARTING':   
                html_info += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"
            elif stat == 'RUNNABLE':   
                html_info += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"            
            elif stat == 'RUNNING':   
                html_info += "<tr class='td_yes'><th>jobName</th><th>startedAt</th><th>jobId</th><th>createdAt</th><th>status</th><th>logs</th></tr>"           
            elif stat == 'FAILED':   
                html_info += "<tr class='td_yes'><th>statusReason</th><th>jobName</th><th>stoppedAt</th><th>startedAt</th><th>jobId</th><th>createdAt</th><th>status</th><th>logs</th></tr>"           
            elif stat == 'SUBMITTED':   
                html_info += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"
            elif stat == 'PENDING':   
                html_info += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"

            html_info += "</table>"

            html_info += "</div>"     
            
        informacion += html_info + "</div>"
        informacion += "<div id='describe_job'></div>"

        total = "<br>" + cabecera + "<br>" + informacion
        data = {'square': total}
        try:           
            data = jsonify(data)
        except RuntimeError as e:
            data = json.dumps(data)
        #print(data)
        return data    
    except Exception as e:
        print(e)


@app.route('/describe_lambdas',methods=['GET','POST'],cors=cors_config, content_types=['application/json'])
@app_flask.route('/describe_lambdas/',endpoint='flask_2',methods=['GET','POST'])
def describe_lambdas():    
    print("The /describe_lambdas function is running.")
    try:
        """ Muestra las descripciones de las colas de trabajo
        """
        cabecera = "<span style='color:red; font-weight: bold;'>"   
        
        informacion ="<form class='form-inline'><div class='form-group'>"
        informacion +="<label for='selection1'>Select Job Queue Name : </label>"
        
        informacion += "<select class='form-control jobQueueName' id='selection1'>"
        
        lines = get_describe_lambdas()
        for line in lines:
            try:            
                informacion+="<option value='" + str(line.rstrip()) + "'>" + str(line.rstrip()) + "</option>"
            except: 
                pass

        cabecera += "</span>"  

        informacion += "</select></div></form>"
        informacion += "<br><br><br>"
        informacion += "<div id='describe_job_queues_status'>"

        informacion += """<div class='tab'>\
        <button class='tablinks' onclick='obtenerlogs(\"SUBMITTED\")' id='defaultOpen'>SUBMITTED</button>\
        </div>"""

        status = ['SUBMITTED']

        html_info = ""

        for stat in status:
                
            html_info += "<div id='" + stat + "' class='tabcontent'>"
            
            html_info += "<table id='t02'>"      
            html_info += "</table>"
                    
            html_info += "<br>"
            html_info += "<table id='t01'>"

            html_info += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"

            html_info += "</table>"

            html_info += "</div>"     
            
        informacion += html_info + "</div>"
        informacion += "<div id='describe_job'></div>"

        total = "<br>" + cabecera + "<br>" + informacion
        data = {'square': total}
        
        try:
            data = jsonify(data)
        except RuntimeError as e:
            data = json.dumps(data)        
        print(data)
        return data
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback_details = {
            'filename': exc_traceback.tb_frame.f_code.co_filename,
            'lineno': exc_traceback.tb_lineno,
            'name': exc_traceback.tb_frame.f_code.co_name,
            'type': exc_type.__name__,
            'message': exc_value.message,
        }
        print("Error : " + str(e))
        print(traceback_details)

def get_list_jobs(jobqueue_name,status):
    """ Obtener los trabajos
    """
    if jobqueue_name:        
        batch = boto3.client('batch')    

        list_jobs = batch.list_jobs(jobQueue = jobqueue_name,jobStatus = status)['jobSummaryList']

        queues = [ {'statusReason':j.get('statusReason',''),'jobName':j['jobName'],'stoppedAt':j.get('stoppedAt',''),'startedAt':j.get('startedAt',''),'jobId':j['jobId'],'createdAt':j['createdAt'],'status':j['status']} for j in list_jobs]
        
        return queues
    else:
        return None

def get_list_logs(jobqueue_name):
    """ Obtener los trabajos
    """
    next_token = None

    client = boto3.client('logs')

    array_names = {}

    while True:
        if next_token:
            response = client.describe_log_streams(logGroupName='/aws/lambda/' + jobqueue_name,nextToken=next_token)
        else:
            response = client.describe_log_streams(logGroupName='/aws/lambda/' + jobqueue_name)

        next_token = response.get('nextToken', None)

        for log_stream in response['logStreams']:
            array_names.update({'creationTime':log_stream['creationTime'],
                                'logStreamName':log_stream['logStreamName'],
                                })    
       
        if not next_token or len(response['logStreams']) == 0:
            break

    print(array_names)
    return array_names



@app.route('/describe_job_queues_status', methods=['GET','POST'],cors=cors_config, content_types=['application/json'])
@app_flask.route('/describe_job_queues_status/',endpoint='flask_3',methods=['GET','POST'])
def describe_job_queues_status():
    """ Mostrar los trabajos
    """
    job_queues_id = str(request.form.get('job_queues_id', ''))    
    status = str(request.form.get('status', ''))    
     
    informacion_ = ""
        
    informacion = "<table id='t03'>"

    if status == 'SUCCEEDED': 
        informacion += "<tr class='td_yes'><th>statusReason</th><th>jobName</th><th>stoppedAt</th><th>startedAt</th><th>jobId</th><th>createdAt</th><th>status</th><th>logs</th></tr>"
        
    elif status == 'STARTING':   
        informacion += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"
        
    elif status == 'RUNNABLE':   
        informacion += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"            
        
    elif status == 'RUNNING':   
        informacion += "<tr class='td_yes'><th>jobName</th><th>startedAt</th><th>jobId</th><th>createdAt</th><th>status</th><th>logs</th></tr>"           
        
    elif status == 'FAILED':   
        informacion += "<tr class='td_yes'><th>statusReason</th><th>jobName</th><th>stoppedAt</th><th>startedAt</th><th>jobId</th><th>createdAt</th><th>status</th><th>logs</th></tr>"           
        
    elif status == 'SUBMITTED':   
        informacion += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"            
        
    elif status == 'PENDING':   
        informacion += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"            
    
    cabecera = "<span style='color:red; font-weight: bold;'>"
    
    lines =get_list_jobs(job_queues_id,status)
    
    print(lines)
    
    if lines:
        for x in lines:

            if status == 'SUCCEEDED':    
                #statusReason              
                informacion+="<tr class='td_yes'><td>" + str(x.get('statusReason','')) + "</td>"                                       
                #jobName                  
                informacion+="<td class='td_yes'>" + str(x.get('jobName','')) + "</td>"                                                   
                #stoppedAt
                unix_timestamp  = printDate(int(str(x.get('stoppedAt',''))))                                     
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"                                                    
                #startedAt
                unix_timestamp  = printDate(int(str(x.get('startedAt',''))))                                     
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"                                                    
                #jobId
                informacion+="<td class='td_yes'>" + str(x.get('jobId','')) + "</td>"                                                    
                #createdAt
                unix_timestamp  = printDate(int(str(x.get('createdAt',''))))         
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"
                #status
                informacion+="<td class='td_yes'>" + str(status) + "</td>"
                informacion+="<td>" + "<a id='" + str(x.get('jobId','')) + "' class='btn btn-primary btn-sm' href='#' onclick='describe_job(this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
                informacion+="</tr>"                                      
            elif status == 'STARTING':    
                #jobName                  
                informacion+="<td class='td_yes'>" + str(x.get('jobName','')) + "</td>"                                                                          
                #createdAt
                unix_timestamp  = printDate(int(str(x.get('createdAt',''))))         
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"  
                #jobId
                informacion+="<td class='td_yes'>" + str(x.get('jobId','')) + "</td>"                                                                                                                                        
                #status
                informacion+="<td class='td_yes'>" + str(status) + "</td>"
                informacion+="<td>" + "<a id='" +  str(x.get('jobId','')) + "' class='btn btn-primary btn-sm' href='#' onclick='describe_job(this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
                informacion+="</tr>"                               
            elif status == 'RUNNABLE':    
                #jobName                  
                informacion+="<td class='td_yes'>" + str(x.get('jobName','')) + "</td>"                                                                          
                #createdAt
                unix_timestamp  = printDate(int(str(x.get('createdAt',''))))         
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"  
                #jobId
                informacion+="<td class='td_yes'>" + str(x.get('jobId','')) + "</td>"                                                                                                                                        
                #status
                informacion+="<td class='td_yes'>" + str(status) + "</td>"
                informacion+="<td>" + "<a id='" +  str(x.get('jobId','')) + "' class='btn btn-primary btn-sm' href='#' onclick='describe_job(this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
                informacion+="</tr>"    
            elif status == 'RUNNING':    
                #jobName                  
                informacion+="<td class='td_yes'>" + str(x.get('jobName','')) + "</td>"                 
                #startedAt
                unix_timestamp  = printDate(int(str(x.get('startedAt',''))))                                     
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"                                        
                #jobId
                informacion+="<td class='td_yes'>" + str(x.get('jobId','')) + "</td>"               
                #createdAt
                unix_timestamp  = printDate(int(str(x.get('createdAt',''))))         
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"                                         
                #status
                informacion+="<td class='td_yes'>" + str(status) + "</td>"
                informacion+="<td>" + "<a id='" + str(x.get('jobId','')) + "' class='btn btn-primary btn-sm' href='#' onclick='describe_job(this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
                informacion+="</tr>"
            elif status == 'FAILED':  
                #statusReason              
                informacion+="<tr class='td_yes'><td>" + str(x.get('statusReason','')) + "</td>"                                       
                #jobName                  
                informacion+="<td class='td_yes'>" + str(x.get('jobName','')) + "</td>"                                                   
                #stoppedAt            
                unix_timestamp  = '' if len(str(x.get('stoppedAt',''))) == 0 else printDate(int(str(x.get('stoppedAt',''))))
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"                                                    
                #startedAt
                unix_timestamp  = '' if len(str(x.get('startedAt',''))) == 0 else printDate(int(str(x.get('startedAt',''))))                                     
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"                                                    
                #jobId
                informacion+="<td class='td_yes'>" + str(x.get('jobId','')) + "</td>"                                                    
                #createdAt
                unix_timestamp  = printDate(int(str(x.get('createdAt',''))))         
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"
                #status
                informacion+="<td class='td_yes'>" + str(status) + "</td>"
                informacion+="<td>" + "<a id='" + str(x.get('jobId','')) + "' class='btn btn-primary btn-sm' href='#' onclick='describe_job(this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
                informacion+="</tr>"                                     
            elif status == 'SUBMITTED':    
                #jobName                  
                informacion+="<td class='td_yes'>" + str(x.get('jobName','')) + "</td>"                                                                          
                #createdAt
                unix_timestamp  = printDate(int(str(x.get('createdAt',''))))         
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"  
                #jobId
                informacion+="<td class='td_yes'>" + str(x.get('jobId','')) + "</td>"                                                                                                                                        
                #status
                informacion+="<td class='td_yes'>" + str(status) + "</td>"
                informacion+="<td>" + "<a id='" +  str(x.get('jobId','')) + "' class='btn btn-primary btn-sm' href='#' onclick='describe_job(this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
                informacion+="</tr>"    
            elif status == 'PENDING':    
                #jobName                  
                informacion+="<td class='td_yes'>" + str(x.get('jobName','')) + "</td>"                                                                          
                #createdAt
                unix_timestamp  = printDate(int(str(x.get('createdAt',''))))         
                informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"  
                #jobId
                informacion+="<td class='td_yes'>" + str(x.get('jobId','')) + "</td>"                                                                                                                                        
                #status
                informacion+="<td class='td_yes'>" + str(status) + "</td>"
                informacion+="<td>" + "<a id='" +  str(x.get('jobId','')) + "' class='btn btn-primary btn-sm' href='#' onclick='describe_job(this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
                informacion+="</tr>"    
    else:
        informacion+="There is not job selected."

    cabecera += "</span>"
    informacion += "</table>"
    informacion_ += cabecera + "<br>" + informacion

    total = "<br>" + informacion_
    data = {'square': total}
    try:
        data = jsonify(data)
    except RuntimeError as e:
        data = json.dumps(data)    
    print(data)
    return data



@app.route('/describe_lambda_group', methods=['GET','POST'],cors=cors_config, content_types=['application/json'])
@app_flask.route('/describe_lambda_group/',endpoint='flask_4',methods=['GET','POST'])
def describe_lambda_group():
    """ Mostrar los trabajos
    """
    job_queues_id = str(request.form.get('job_queues_id', ''))    
      
    informacion_ = ""
        
    informacion = "<table id='t03'>"

    informacion += "<tr class='td_yes'><th>jobName</th><th>createdAt</th><th>jobId</th><th>status</th><th>logs</th></tr>"            
            
    cabecera = "<span style='color:red; font-weight: bold;'>"
    
    lines =get_list_logs(job_queues_id)
    
    print(lines)
    
    for x in lines:
                                             
        #jobName                  
        informacion+="<td class='td_yes'>" + str(x.get('jobName','')) + "</td>"                                                                          
        #createdAt
        unix_timestamp  = printDate(int(str(x.get('createdAt',''))))         
        informacion+="<td class='td_yes'>" + str(unix_timestamp) + "</td>"  
        #jobId
        informacion+="<td class='td_yes'>" + str(x.get('jobId','')) + "</td>"                                                                                                                                        
        #status
        informacion+="<td class='td_yes'>" + str(status) + "</td>"
        informacion+="<td>" + "<a id='" +  str(x.get('jobId','')) + "' class='btn btn-primary btn-sm' href='#' onclick='describe_job(this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
        informacion+="</tr>"           

    cabecera += "</span>"
    informacion += "</table>"
    informacion_ += cabecera + "<br>" + informacion

    total = "<br>" + informacion_
    data = {'square': total}
    try:
        data = jsonify(data)
    except RuntimeError as e:
        data = json.dumps(data)    
    print(data)
    return data


def get_describe_jobs(jobid):
    """ Obtener los logs de los trabajos
    """
    try:
        batch = boto3.client('batch')    
        
        queues = [q['container']['logStreamName'] for q in batch.describe_jobs(jobs=[jobid])['jobs']]
        
        return queues
    except Exception as e:
        return None

@app.route('/describe_job', methods=['GET','POST'],cors=cors_config, content_types=['application/json'])
@app_flask.route('/describe_job/',endpoint='flask_5',methods=['GET','POST'])
def describe_job():
    """ Mostrar los logs de los trabajos
    """
    
    jobid = str(request.form.get('jobid', ''))    
  
    informacion_ = ""
        
    informacion = "<table id='t01'>"

    cabecera = "<span style='color:red; font-weight: bold;'>"
            
    lines = get_describe_jobs(jobid)

    if lines:
        for line in lines:

            jobid = ""    

            try:
                #statusReason              
                informacion+="<tr class='td_yes'><td>" + str(line.rstrip()) + "</td>"
                informacion+="<td>" + "<a id='" + str(line.rstrip()) + "' class='btn btn-primary btn-sm' href='#' onclick='MyFunction(\"/aws/batch/job\",this.id);return false;'><span class='glyphicon glyphicon-search'></span></a>" + "</td>"
                informacion+="</tr>"                                                        
            except: 
                pass
    else:
        informacion+="The job has not yet generated any log."
    cabecera += "</span>"
    informacion += "</table>"
    informacion_ += cabecera + "<br>" + informacion

    total = "<br>" + informacion_
    data = {'square': total}
    try:
        data = jsonify(data)
    except RuntimeError as e:
        data = json.dumps(data)    
    print(data)
    return data  

@app.route('/square', methods=['GET','POST'],cors=cors_config, content_types=['application/json'])
@app_flask.route('/square/',endpoint='flask_6',methods=['GET','POST'])
def square():
    """ Mostrar el contenido del log
    """
    if flask.request.method == 'POST':        
        loggroupname_name = str(request.form.get('loggroupname_name', ''))            
        logstreamname_name = str(request.form.get('logstreamname_name', '').replace(' ',''))
    else:
        pass

    print(loggroupname_name)
    print(logstreamname_name)

    cabecera = "<table id='t02'>"       
    informacion = "<p>"
    lines = get_log_events(loggroupname_name,logstreamname_name,"us-east-1")

    for line in lines:
        try:
            
            informacion+="" + line.rstrip() + "<br><br>"
        except: 
            pass

    cabecera += "</table>"
    
    informacion += "</p>"

    total = "<br>" + cabecera + "<br>" + informacion
    data = {'square': total}
    try:
        data = jsonify(data)
    except RuntimeError as e:
        data = json.dumps(data)    
    print(data)
    return data

def get_log_events(log_group_name,log_stream_name,region):
    """ Obtener el contenido del log
    :param log_group_name: grupo de log
    :param log_stream_name: nombre del log
    """
    client = boto3.client('logs',region_name=region)

    print(region)
    print(log_group_name)
    print(log_stream_name)

    logs_batch = client.get_log_events(logGroupName=log_group_name, logStreamName=log_stream_name)
    
    logs_events = []
    for event in logs_batch['events']:
        ev = json.dumps(event['message'])
        if "MemoryStore" not in ev and \
            "SparkUI" not in ev and \
            "ShutdownHookManager" not in ev and \
            "BlockManager" not in ev and \
            "StateStoreCoordinatorRef" not in ev and \
            "AbstractConnector" not in ev and \
            "OutputCommitCoordinator" not in ev and \
            "MapOutputTrackerMasterEndpoint" not in ev and \
            "ContextCleaner" not in ev and \
            "ParquetOutputFormat" not in ev and \
            "TaskSetManager" not in ev and \
			"DAGScheduler" not in ev and \
            "SparkHadoopMapRedUtil" not in ev and \
            "FileSourceStrategy" not in ev and \
            "SQLHadoopMapReduceCommitProtocol" not in ev and \
            "FileOutputCommitter" not in ev and \
            "ParquetFileFormat" not in ev and \
            "FileSourceStrategy" not in ev and \
            "TaskSchedulerImpl" not in ev and \
            "PrunedInMemoryFileIndex" not in ev and \
            "FileSourceScanExec" not in ev and \
            "CodecPool" not in ev and \
            "SecurityManager" not in ev and \
            "ContextHandler" not in ev and \
            "ShuffleBlockFetcherIterator" not in ev and \
            "CodeGenerator" not in ev and \
            "CodecConfig" not in ev and \
            "SessionState" not in ev and \
            "HiveClientImpl" not in ev and \
            "lrwxrwxrwx" not in ev and \
            "drwxr-xr-x" not in ev and \
            "dr-xr-xr-x" not in ev and \
            "drwx------" not in ev and \
            "-rw-r--r--" not in ev and \
            "-rwxrwxrwx" not in ev and \
            "drwx------" not in ev and \
            "drwxrwxrwx" not in ev and \
            "drwxrwxrwt" not in ev and \
            "SparkContext" not in ev:            
            
            logs_events.append(ev)
        
    while 'nextToken' in logs_batch:
        logs_batch = client.get_log_events(logGroupName=log_group_name, logStreamName=log_stream_name, nextToken=logs_batch['nextToken'])
        for event in logs_batch['events']:
            ev = json.dumps(event['message'])
            if "MemoryStore" not in ev and \
            "SparkUI" not in ev and \
            "ShutdownHookManager" not in ev and \
            "BlockManager" not in ev and \
            "StateStoreCoordinatorRef" not in ev and \
            "AbstractConnector" not in ev and \
            "OutputCommitCoordinator" not in ev and \
            "MapOutputTrackerMasterEndpoint" not in ev and \
            "ContextCleaner" not in ev and \
            "ParquetOutputFormat" not in ev and \
            "TaskSetManager" not in ev and \
			"DAGScheduler" not in ev and \
            "SparkHadoopMapRedUtil" not in ev and \
            "FileSourceStrategy" not in ev and \
            "SQLHadoopMapReduceCommitProtocol" not in ev and \
            "FileOutputCommitter" not in ev and \
            "ParquetFileFormat" not in ev and \
            "FileSourceStrategy" not in ev and \
            "TaskSchedulerImpl" not in ev and \
            "PrunedInMemoryFileIndex" not in ev and \
            "FileSourceScanExec" not in ev and \
            "CodecPool" not in ev and \
            "SecurityManager" not in ev and \
            "ContextHandler" not in ev and \
            "ShuffleBlockFetcherIterator" not in ev and \
            "CodeGenerator" not in ev and \
            "CodecConfig" not in ev and \
            "SessionState" not in ev and \
            "HiveClientImpl" not in ev and \
            "lrwxrwxrwx" not in ev and \
            "drwxr-xr-x" not in ev and \
            "dr-xr-xr-x" not in ev and \
            "drwx------" not in ev and \
            "-rw-r--r--" not in ev and \
            "-rwxrwxrwx" not in ev and \
            "drwx------" not in ev and \
            "drwxrwxrwx" not in ev and \
            "drwxrwxrwt" not in ev and \
            "SparkContext" not in ev:
                logs_events.append(ev)
    
    print(logs_events)
    return logs_events

def printDate(timestamp):
    """ Convertir una fecha timestamp en texto
    :param timestamp: Ejm: 1559736173031 
    """
    print(str(timestamp))
    return datetime.datetime.fromtimestamp(timestamp/1000).strftime("%Y-%m-%d %H:%M:%S")    

if __name__ == '__main__':
    app_flask.run(debug=True)
    

# The view function above will return {"hello": "world"}
# whenever you make an HTTP GET request to '/'.
#
# Here are a few more examples:
#
# @app.route('/hello/{name}')
# def hello_name(name):
#    # '/hello/james' -> {"hello": "james"}
#    return {'hello': name}
#
# @app.route('/users', methods=['POST'])
# def create_user():
#     # This is the JSON body the user sent in their POST request.
#     user_as_json = app.current_request.json_body
#     # We'll echo the json body back to the user in a 'user' key.
#     return {'user': user_as_json}
#
# See the README documentation for more examples.
#
